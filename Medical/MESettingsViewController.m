//
//  MESettingsViewController.m
//  Medical
//
//  Created by Victor Schepanovsky on 8/28/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MESettingsViewController.h"
#import "MESettings.h"
#import "MELocalization.h"

@interface MESettingsViewController ()

@end

@implementation MESettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = [MELocalization localizedString:@"Settings"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:[MELocalization localizedString:@"Done"]
                                     style:UIBarButtonItemStyleBordered
                                    target:self
                                    action:@selector(doneAction)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithRed:74/255.0 green:171/255.0 blue:113/255.0 alpha:1];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.langLabel.text = [MELocalization localizedString:@"Language"];
    self.langSelection.selectedSegmentIndex = [[MESettings settings] language];
    self.phoneField.text = [[MESettings settings] phone];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)languageChanged:(id)sender
{
//    [[MESettings settings] setLanguage:self.langSelection.selectedSegmentIndex];
}


- (void) doneAction
{
    [[MESettings settings] setLanguage:(MELanguage)self.langSelection.selectedSegmentIndex];
    [[MESettings settings] setPhone:self.phoneField.text];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
