//
//  MECategoryViewController.m
//  Medical
//
//  Created by Victor Schepanovsky on 9/1/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MECategoryViewController.h"
#import "MESettingsViewController.h"
#import "MELocalization.h"
#import "MEContentViewController.h"

@interface MECategoryViewController () <UISearchBarDelegate, UIAlertViewDelegate>

@end

@implementation MECategoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //self.categories = @[@"subcat1", @"subcat2"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 19, 14)];
    [button setImage:[UIImage imageNamed:@"btn_settings"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(showSettings:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    if (self.navigationController.viewControllers.count > 1)
    {
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)]; // 10 x 16
        backButton.imageEdgeInsets = UIEdgeInsetsMake(14, 0, 14, 34);
        [backButton setImage:[UIImage imageNamed:@"ico_arrow_back.png"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    }
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void) viewWillAppear:(BOOL)animated
{
//    self.navigationItem.titleView = self.searchBar;
//    self.searchDisplayController.displaysSearchBarInNavigationBar = YES;
//    self.searchBar.placeholder = [MELocalization localizedString:@"Search"];
//    self.searchBar.delegate = self;
    self.title = [MELocalization localizedString:self.categoryObject.title];
    [self.callButton setTitle:[MELocalization localizedString:@"Call"] forState:UIControlStateNormal];
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view delegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 68;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.categoryObject.subcategories.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseId = @"reuseId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico_arrow.png"]];
        imageView.frame = CGRectMake(0, 0, 10, 16);
        cell.accessoryView = imageView;
        cell.backgroundColor = [UIColor whiteColor];
    }
    cell.textLabel.font = [UIFont boldSystemFontOfSize:25];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.textLabel.text = [@"  " stringByAppendingString:[MELocalization localizedString:[self.categoryObject.subcategories objectAtIndex:indexPath.row]]];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.searchBar resignFirstResponder];
    
    NSString *key = [self.categoryObject.subcategories objectAtIndex:indexPath.row];
    MECategory *category = [[MECategory alloc] initWithKey:key];
    if (category.subcategories.count > 0)
    {
        MECategoryViewController *categoryController = [MECategoryViewController new];
        categoryController.categoryObject = category;
        [self.navigationController pushViewController:categoryController animated:YES];
    }
    else
    {
        MEContentViewController *contentController = [MEContentViewController new];
        MEArticle *article = [[MEArticle alloc] initWithKey:key];
        contentController.article = article;
        [self.navigationController pushViewController:contentController animated:YES];
    }
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStyleBordered
                                    target:nil
                                    action:nil];
}


#pragma mark - call

- (IBAction)callAction:(id)sender
{
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[MESettings settings] phone] message:[MELocalization localizedString:@"Call?"] delegate:self cancelButtonTitle:[MELocalization localizedString:@"Cancel"] otherButtonTitles:[MELocalization localizedString:@"Call"], nil];
        [alertView show];
    } else {
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", kMECallNumber]]];
    }
}


- (void)showSettings:(id)sender
{
    MESettingsViewController *settingsController = [MESettingsViewController new];
    [self.navigationController pushViewController:settingsController animated:YES];
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStyleBordered
                                    target:nil
                                    action:nil];
}

- (void) backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - search bar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length == 0)
    {
        [searchBar resignFirstResponder];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

@end
