//
//  MEContentViewController.h
//  Medical
//
//  Created by Victor Schepanovsky on 9/8/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEArticle.h"
#import "SMPageControl.h"

@interface MEContentViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) SMPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIButton *callButton;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIView *line;
@property (nonatomic, weak) IBOutlet UIView *pageControlPlaceholder;



@property (nonatomic, strong) NSMutableArray *controllers;
@property (nonatomic, strong) MEArticle *article;
@property (nonatomic, strong) UIView *contentView;

- (IBAction)callAction:(id)sender;
- (IBAction)backAction:(id)sender;

@end
