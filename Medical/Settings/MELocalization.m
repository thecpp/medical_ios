//
//  MELocalization.m
//  Medical
//
//  Created by Victor Schepanovsky on 9/1/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MELocalization.h"

@implementation MELocalization

static NSArray *sLanguages;

+ (NSString *) localizedString:(NSString *)string
{
    return [self localizedString:string language:[[MESettings settings] language]];
}

+ (NSString *) localizedString:(NSString *)string language:(MELanguage)language
{
    NSString *result = [[sLanguages objectAtIndex:language] objectForKey:string];
    return result ? result : string;
}

+ (void) initialize
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"0" ofType:@"strings"];
    NSDictionary *stringDictionary = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    NSString *filePath2 = [[NSBundle mainBundle] pathForResource:@"1" ofType:@"strings"];
    NSDictionary *stringDictionary2 = [NSDictionary dictionaryWithContentsOfFile:filePath2];

    sLanguages = @[stringDictionary, stringDictionary2];
}

@end
