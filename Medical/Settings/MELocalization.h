//
//  MELocalization.h
//  Medical
//
//  Created by Victor Schepanovsky on 9/1/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MESettings.h"

@interface MELocalization : NSObject

+ (NSString *) localizedString:(NSString *)string;
+ (NSString *) localizedString:(NSString *)string language:(MELanguage)language;

@end
