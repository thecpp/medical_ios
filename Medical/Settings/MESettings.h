//
//  MESettings.h
//  Medical
//
//  Created by Victor Schepanovsky on 9/1/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kMEUkrainian = 0,
    kMERussian = 1
} MELanguage;

@interface MESettings : NSObject
@property (nonatomic, assign) MELanguage language;
@property (nonatomic, strong) NSString *phone;

+ (id) settings;

@end
