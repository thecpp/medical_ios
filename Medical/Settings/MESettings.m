//
//  MESettings.m
//  Medical
//
//  Created by Victor Schepanovsky on 9/1/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MESettings.h"

@implementation MESettings

+ (id) settings
{
    static MESettings *sSettings;
    if (!sSettings)
    {
        sSettings = [MESettings new];
    }
    return sSettings;
}

- (id) init
{
    self = [super init];
    if (self)
    {
        _language = (MELanguage)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
        _phone = [[NSUserDefaults standardUserDefaults] stringForKey:@"phone"];
        if (!_phone)
        {
            _phone = kMECallNumber;
        }
    }
    return self;
}

- (void) setLanguage:(MELanguage)language
{
    _language = language;
    [[NSUserDefaults standardUserDefaults] setInteger:_language forKey:@"language"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setPhone:(NSString *)phone
{
    _phone = phone;
    [[NSUserDefaults standardUserDefaults] setObject:_phone forKey:@"phone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
