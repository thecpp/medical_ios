//
//  MEAppDelegate.h
//  Medical
//
//  Created by Victor Schepanovsky on 8/20/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
