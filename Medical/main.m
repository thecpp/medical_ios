//
//  main.m
//  Medical
//
//  Created by Victor Schepanovsky on 8/20/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MEAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MEAppDelegate class]));
    }
}
