//
//  MEPageViewController.h
//  Medical
//
//  Created by Victor Schepanovsky on 9/10/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEArticle.h"

@protocol MEScrollingResponder <NSObject>

- (void) scrollViewDidScrollToResponder:(UIScrollView *)scrollView;

@end

@interface MEPageViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UITextView *textView;

@property (nonatomic, strong) MEArticle *article;
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, weak) id<MEScrollingResponder> scrollingResponder;

@end
