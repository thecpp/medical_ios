//
//  MEReaderController.m
//  Medical
//
//  Created by Victor Schepanovsky on 8/21/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MEReaderController.h"

@interface MEReaderController ()

@end

@implementation MEReaderController


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *path = [[NSBundle mainBundle] pathForResource:self.fileName ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:htmlString baseURL:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
