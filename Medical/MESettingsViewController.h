//
//  MESettingsViewController.h
//  Medical
//
//  Created by Victor Schepanovsky on 8/28/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MESettingsViewController : UIViewController
@property (nonatomic, weak) IBOutlet UISegmentedControl *langSelection;
@property (nonatomic, weak) IBOutlet UILabel *langLabel;
@property (nonatomic, weak) IBOutlet UILabel *phoneLabel;
@property (nonatomic, weak) IBOutlet UITextField *phoneField;

- (IBAction)languageChanged:(id)sender;

@end
