//
//  MEPDFViewController.h
//  Medical
//
//  Created by Victor Schepanovsky on 8/22/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEPDFViewController : UIViewController
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@end
