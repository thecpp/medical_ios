//
//  MEPageViewController.m
//  Medical
//
//  Created by Victor Schepanovsky on 9/10/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MEPageViewController.h"

@interface MEPageViewController () <UIScrollViewDelegate>

@end

@implementation MEPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.scrollView.backgroundColor = [UIColor redColor];

}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.contentView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.contentView.frame.size.height);
    self.scrollView.contentSize = self.contentView.frame.size;
    [self.scrollView addSubview:self.contentView];
    
    NSString *text = [[self.article contentForPage:self.page+1] stringByAppendingString:@"\n"];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setLineSpacing:7] ;
    [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
    [attrString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, text.length)];
    [self.textView setScrollEnabled:YES];
    self.textView.attributedText = attrString;
    [self.textView setScrollEnabled:NO];
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%ld.png", self.article.title, (long)self.page+1]];
    if (image)
    {
        self.imageView.image = image;

        CGSize size = [self.textView sizeThatFits:CGSizeMake(self.textView.frame.size.width - 8, 10000)];
        float delta = size.height - self.textView.frame.size.height;// + 64;
        
        CGRect frame = self.textView.frame;
        frame.size.height += delta;
        self.textView.frame = frame;
        
        frame = self.contentView.frame;
        frame.size.height += delta;
        self.contentView.frame = frame;
        self.scrollView.contentSize = self.contentView.frame.size;
        self.textView.userInteractionEnabled = NO;
    }
    else
    {
        CGSize size = [self.textView sizeThatFits:CGSizeMake(self.textView.frame.size.width - 8, 10000)];
        //self.textView.backgroundColor = [UIColor redColor];
        self.contentView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, size.height);
        self.scrollView.contentSize = self.contentView.frame.size;
        self.textView.frame = CGRectMake(4, 0, self.scrollView.frame.size.width - 8, size.height);
        //[self.textView setScrollEnabled:YES];
    }
    
    self.scrollView.delegate = self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - scroll delegate

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.scrollingResponder && [self.scrollingResponder respondsToSelector:@selector(scrollViewDidScrollToResponder:)])
    {
        [self.scrollingResponder scrollViewDidScrollToResponder:scrollView];
    }
}


@end
