//
//  MEContentViewController.m
//  Medical
//
//  Created by Victor Schepanovsky on 9/8/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MEContentViewController.h"
#import "MELocalization.h"
#import "MEPageViewController.h"

@interface MEContentViewController () <UIScrollViewDelegate, MEScrollingResponder>

@end

@implementation MEContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.controllers = [NSMutableArray new];
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    return self;
}

- (void) createPageControl
{
    self.pageControl = [[SMPageControl alloc] initWithFrame:self.pageControlPlaceholder.bounds];
    [self.pageControlPlaceholder addSubview:self.pageControl];
    self.pageControl.hidesForSinglePage = YES;
    self.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:74/255.0 green:171/255.0 blue:113/255.0 alpha:1];
    self.pageControl.pageIndicatorTintColor = [UIColor grayColor];
    self.pageControl.indicatorDiameter = 16;
    self.pageControl.numbered = YES;
    [self.pageControl addTarget:self action:@selector(pageChanged:) forControlEvents:UIControlEventValueChanged];

}



- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.article)
    {
        self.titleLabel.text = [MELocalization localizedString:self.article.title];
    }
    
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self createPageControl];
    
    self.line.frame = CGRectMake(0, self.line.frame.origin.y, self.line.frame.size.width, 0.5f);
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    // page control showing
    //float height = 12;
    //self.pageControl.frame = CGRectMake(0, self.navigationController.navigationBar.frame.size.height - height, self.navigationController.navigationBar.frame.size.width, height);
    //[self.navigationController.navigationBar addSubview:self.pageControl];
    self.pageControl.alpha = 0;
    [UIView animateWithDuration:0.3f animations:^{
        self.pageControl.alpha = 1;
    }];
    
    [self createPages];

    
    // call button showing
//    self.callButton.center = CGPointMake(self.view.center.x, self.view.frame.size.height - self.callButton.frame.size.height + 96);
//    [self.view addSubview:self.callButton];

    self.contentView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width * self.article.pages, [UIScreen mainScreen].bounds.size.height - self.scrollView.frame.origin.y);
    self.scrollView.contentSize = self.contentView.frame.size;

}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    self.contentView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width * self.article.pages, self.scrollView.frame.size.height);
//    self.scrollView.contentSize = self.contentView.frame.size;
}


- (void) viewWillDisappear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    self.scrollView.delegate = nil;
    [UIView animateWithDuration:0.1f animations:^{
        self.pageControl.alpha = 0;
    } completion:^(BOOL finished) {
        if (self.pageControl) {
            [self.pageControl removeFromSuperview];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setArticle:(MEArticle *)article
{
    _article = article;
}


#pragma mark - call

- (IBAction)callAction:(id)sender
{
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", kMECallNumber]]];
    }
    else
    {
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - create pages

- (void) createPages
{
    self.pageControl.numberOfPages = self.article.pages;
    //self.pageControl.
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.frame.size.width * self.article.pages, self.scrollView.frame.size.height)];
    //self.contentView.backgroundColor = [UIColor redColor];
    self.scrollView.contentSize = self.contentView.frame.size;
    
    for (int i = 0; i < self.article.pages; i++)
    {
        MEPageViewController *pageController = [MEPageViewController new];
        pageController.article = self.article;
        pageController.scrollingResponder = self;
        pageController.page = i;
        [self.controllers addObject:pageController];
        
        pageController.view.frame = CGRectMake(self.scrollView.frame.size.width * i, 0, self.scrollView.frame.size.width, self.contentView.frame.size.height);
        [self addChildViewController:pageController];
        [self.contentView addSubview:pageController.view];
    }
    [self.scrollView addSubview:self.contentView];
}

#pragma mark - scroll delegate

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = (int) ((scrollView.contentOffset.x  + scrollView.frame.size.width / 2) / scrollView.frame.size.width);
    self.pageControl.currentPage = page;
}

- (void) scrollViewDidScrollToResponder:(UIScrollView *)scrollView
{
    // change bar size
    
    float diff = MIN(20, MAX(0, scrollView.contentOffset.y));
    self.pageControl.numbered = (diff < 5);
    self.pageControl.indicatorDiameter = 16 - diff/2;
    //NSLog(@"%f", scrollView.contentOffset.y);
    
}

#pragma mark - page selected

- (void) pageChanged:(id)sender
{
    CGPoint offset = CGPointMake(self.scrollView.frame.size.width * self.pageControl.currentPage, 0);
    [self.scrollView setContentOffset:offset animated:YES];
}

@end
