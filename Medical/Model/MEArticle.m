//
//  MEArticle.m
//  Medical
//
//  Created by Victor Schepanovsky on 9/1/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MEArticle.h"
#import "MELocalization.h"
#import "MESettings.h"

@interface MEArticle ()
@property (nonatomic, assign) BOOL filesChecked;
@property (nonatomic, assign) NSInteger pages;

@end


@implementation MEArticle

- (BOOL) hasChild
{
    return NO;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}

- (id) initWithDictionary:(NSDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    if (self)
    {
    }
    return self;
}

- (id) initWithKey:(NSString *)key
{
    self = [self init];
    if (self)
    {
        self.title = key;
    }
    return self;
}

- (NSString *) contentForPage:(NSInteger)page
{
    NSString *result = nil;
    NSError *error = nil;

    NSString *languagePostfix = [[MESettings settings] language] == kMERussian ? @"_ru" : @"";
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@%ld%@", self.title, (long)page, languagePostfix] ofType:@"txt"];
    NSData *data = [NSData dataWithContentsOfFile:path options:NSDataReadingMapped error:&error];
    if (error)
    {
        NSLog(@"%@", error);
    }
    else
    {
        result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    return result;
}

- (NSInteger) pagesCount
{
    NSInteger pagesCount = 1;
    while (nil != [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@%ld", self.title, (long)pagesCount+1] ofType:@"txt"])
    {
        pagesCount++;
    }
    return pagesCount;
}


- (NSInteger) pages
{
    if (!self.filesChecked)
    {
        _pages = [self pagesCount];
        self.filesChecked = YES;
    }
    return _pages;
}

@end
