//
//  MENode.m
//  Medical
//
//  Created by Victor Schepanovsky on 9/1/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MENode.h"

@implementation MENode

static NSDictionary *sLibrary;


- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_title forKey:@"title"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self)
    {
        self.title = [aDecoder decodeObjectForKey:@"title"];
    }
    return self;
}

- (id) initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    if (self)
    {
        self.title = [dictionary objectForKey:@"title"];
    }
    return self;
}

- (id) initWithKey:(NSString *)key
{
    if (!sLibrary)
    {
        NSError *error = nil;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"json"];
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        sLibrary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        if (error)
        {
            NSLog(@"%@", error);
        }
    }
    NSDictionary *dictionary = [sLibrary objectForKey:key];
    return [self initWithDictionary:dictionary];
}

@end
