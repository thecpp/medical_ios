//
//  MENode.h
//  Medical
//
//  Created by Victor Schepanovsky on 9/1/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MENode : NSObject <NSCoding>
@property (nonatomic,strong) NSString *title;
@property (nonatomic, readonly) BOOL hasChild;

- (id) initWithDictionary:(NSDictionary *)dictionary;
- (id) initWithKey:(NSString *)key;

@end
