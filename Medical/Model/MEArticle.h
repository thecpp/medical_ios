//
//  MEArticle.h
//  Medical
//
//  Created by Victor Schepanovsky on 9/1/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import "MENode.h"

@interface MEArticle : MENode
@property (nonatomic, assign, readonly) NSInteger pages;

- (NSString *) contentForPage:(NSInteger)page;

@end
