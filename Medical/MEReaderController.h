//
//  MEReaderController.h
//  Medical
//
//  Created by Victor Schepanovsky on 8/21/14.
//  Copyright (c) 2014 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEReaderController : UIViewController
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *fileName;
@end
